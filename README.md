# TP_databases

Тестовое задание для реализации проекта “Форумы” на курсе по базам данных в Технопарке Mail.ru

### Ссылки

Github с заданием и тестированием: https://github.com/bozaro/tech-db-forum

Swagger: https://tech-db-forum.bozaro.ru/

### Запуск

`sudo docker build -t akenoq_dz .`

`sudo docker run -p 5000[PC]:5000[FROM] --name akenoq_dz -t akenoq_dz`
